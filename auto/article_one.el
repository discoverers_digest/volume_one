(TeX-add-style-hook
 "article_one"
 (lambda ()
   (LaTeX-add-labels
    "fig:editorial"
    "sec:passion"
    "sec:purpose"
    "sec:experience_everything"))
 :latex)

