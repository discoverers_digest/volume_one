(TeX-add-style-hook
 "article_four"
 (lambda ()
   (LaTeX-add-labels
    "fig:planning"
    "fig:starmap"
    "fig:nasa"
    "fig:fg_scenery"
    "fig:storyboard"))
 :latex)

